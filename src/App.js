import React, { useState } from "react";
import "./App.css";

function App() {
  const [result, setResult] = useState(0);
  const [staging, addToStaging] = useState("");
  const [history, addToHistory] = useState([]);
  const displayResult = () => {
    return !result ? staging : result;
  };

  const betterStaging = number => {
    addToStaging(staging + number);
  };

  const clear = () => {
    setResult("");
    addToStaging("");
  };
  const calculate = () => {
    try {
      setResult(eval(staging));
      addToHistory([...history, { id: history.length, value: staging }]);
      console.log(history);
    } catch (err) {
      setResult(err.message);
    }
  };
  return (
    <div className="App">
      <div className="flexbox">
        <div className="grid">
          <div className="result">{displayResult()}</div>
          <div className="clear" onClick={() => clear()}>
            clear
          </div>
          <div
            className="operation multiply"
            onClick={() => betterStaging("*")}
          >
            x
          </div>
          <div
            className="operation division"
            onClick={() => betterStaging("/")}
          >
            ÷
          </div>
          <div className="num num7" onClick={() => betterStaging(7)}>
            7
          </div>
          <div className="num num8" onClick={() => betterStaging(8)}>
            8
          </div>
          <div className="num num9" onClick={() => betterStaging(9)}>
            9
          </div>
          <div className="operation minus" onClick={() => betterStaging("-")}>
            -
          </div>
          <div className="num num4" onClick={() => betterStaging(4)}>
            4
          </div>
          <div className="num num5" onClick={() => betterStaging(5)}>
            5
          </div>
          <div className="num num6" onClick={() => betterStaging(6)}>
            6
          </div>
          <div className="operation plus" onClick={() => betterStaging("+")}>
            +
          </div>
          <div className="num num1" onClick={() => betterStaging(1)}>
            1
          </div>
          <div className="num num2" onClick={() => betterStaging(2)}>
            2
          </div>
          <div className="num num3" onClick={() => betterStaging(3)}>
            3
          </div>
          <div className="equal" onClick={() => calculate()}>
            =
          </div>
        </div>
        <div className="history">
          <h3>Historique</h3>
          <ul>
            {history.map(h => (
              <li onClick={() => addToStaging(h.value)}>
                {h.value} = {result}
              </li>
            ))}
          </ul>
        </div>
      </div>
    </div>
  );
}

export default App;
